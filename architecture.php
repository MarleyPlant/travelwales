<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <?php include 'parts/head.php'; ?>
        <title>TravelWales - Architecture</title>
    </head>


    <body>
        <?php include 'parts/header.php'; ?>

        <div class="post-list">
            <h1 class="post-list-title">Architecture</h1>

            <?php
                $entry = [
                    'title' => "Cardiff Castle",
                    'img'   => "https://lp-cms-production.imgix.net/2019-06/caa4cd3597e2b315c29d1ee4c8aba28f-cardiff-castle.jpg",
                    'excerpt' => 'Cardiff Castle is a medieval castle and Victorian Gothic revival mansion located in the city centre of Cardiff, Wales. The original motte and bailey castle was built in the late 11th century by Norman invaders on top of a 3rd-century Roman fort',
                    'website' => 'https://www.cardiffcastle.com/',
                    'distance' => ['51.482358227214895','-3.181251803405194'],
                    'directionsLink' => 'https://www.google.com/search?q=cardiff+castle+lat+long&sxsrf=AOaemvJxyiHTZ1UGcEq59NAMPBY4GGuvIQ%3A1638190190281&ei=bsykYeevEM_1gQaGpLvADg&ved=0ahUKEwjnnvaCzr30AhXPesAKHQbSDugQ4dUDCA4&uact=5&oq=cardiff+castle+lat+long&gs_lcp=Cgdnd3Mtd2l6EAMyBQghEKABOgcIABBHELADOgUIABCABDoLCC4QgAQQxwEQrwE6BAgAEEM6BQgAEMQCOgYIABAWEB46CQgAEMkDEBYQHjoLCAAQgAQQsQMQgwE6CAgAEBYQChAeOggIIRAWEB0QHkoECEEYAFDXBFiPGmDnGmgGcAN4AIAB-AGIAekLkgEFMC45LjGYAQCgAQHIAQjAAQE&sclient=gws-wiz#'
                ];

                include 'parts/postlist-entry.php';
            ?>

            <?php
                $entry = [
                    'title' => "Center For Alternative Technology",
                    'img'   => "https://cat.org.uk/app/uploads/2018/11/wise_from_the_air.jpg",
                    'excerpt' => 'To avoid disastrous climate change we must take radical action now. CAT offers practical solutions and hands-on learning to help create a zero carbon world.',
                    'website' => 'https://cat.org.uk/',
                    'distance' => ['52.62305','-3.84062'],
                    'directionsLink' => ''
                ];

                include 'parts/postlist-entry.php';
            ?>
        </div>
    </body>
</html>
let currentSlide = 0;
let dotActiveClass = 'slider-dot-active';

function resetSlides() {
    $(".slider-status-dots>.slider-dot").removeClass(dotActiveClass);
    $(".slider > .slider-img").css("display", "none");
  }

function nextSlide() {
  resetSlides();
  currentSlide++;
  if (currentSlide > slides.length) {
    currentSlide = 1;
  }

  dots[currentSlide - 1].classList.add(dotActiveClass);
  showSlide(currentSlide - 1);
}

function showSlide(slide) {
  slides[slide].style.display = "block";
}

function prevSlide() {
  resetSlides();
  if (currentSlide > 0) { currentSlide--;}   
  else if (currentSlide == 0) {
    currentSlide = slides.length - 1;
  }

  if(currentSlide > 0 ) {
    dots[currentSlide - 1].classList.add(dotActiveClass);
  } else {
    dots[slides.length - 1].classList.add(dotActiveClass);
  }
  showSlide(currentSlide);
}

$(document).ready(function () {
  slides = $(".slider-img");
  dots = $(".slider-dot");

  $("#next").click(() => {
    nextSlide();
  });

  $("#prev").click(() => {
    prevSlide();
  });

  $('.slider-dot').click( async(event) =>
  {
    resetSlides();
    currentSlide = $(event.target).index();
    showSlide(currentSlide);
    event.target.classList.add(dotActiveClass);
  });

  nextSlide();
  setTimeout(nextSlide, 5000); // Change image every 2 seconds
});

![logo](&#x2F;.dev&#x2F;logo.png)
<p>Project Website used for L3 IT Course, created as a site to promote tourism in wales.</p>




![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/MarleyPlant/travelwales?style=flat&branch=master) ![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/MarleyPlant/travelwales?style=flat) <br />

<br />


## 💻 **TECHNOLOGIES**
[![gulp](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;gulp-CF4647?style&#x3D;for-the-badge&amp;logo&#x3D;gulp&amp;logoColor&#x3D;white)]()
[![CSS3](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;CSS3-1572B6?style&#x3D;for-the-badge&amp;logo&#x3D;CSS3&amp;logoColor&#x3D;white)]()
[![HTML5](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;HTML5-E34F26?style&#x3D;for-the-badge&amp;logo&#x3D;HTML5&amp;logoColor&#x3D;white)]()
[![JavaScript](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;JavaScript-F7DF1E?style&#x3D;for-the-badge&amp;logo&#x3D;JavaScript&amp;logoColor&#x3D;white)]()
[![Sass](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Sass-CC6699?style&#x3D;for-the-badge&amp;logo&#x3D;Sass&amp;logoColor&#x3D;white)]()
[![npm](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;npm-CB3837?style&#x3D;for-the-badge&amp;logo&#x3D;npm&amp;logoColor&#x3D;white)]()

<br />


<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <?php include 'parts/head.php'; ?>
        <title>TravelWales - Home</title>
    </head>

    <body>
        <?php include 'parts/header.php'; ?>

        <div class="hero">
            <div class="slider">
                <img class="slider-img" src="img/beach.jpg" alt="Beach slider Photo">
                <img class="slider-img" src="https://images.unsplash.com/photo-1610107318580-5a0973dba70d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=joseph-reeder-INVi7be11S8-unsplash.jpg" alt="Beach slider Photo">
                <img class="slider-img" src="https://images.unsplash.com/photo-1523906947658-bb93228a9c82?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=beata-mitrega-nsiPzi9FVh4-unsplash.jpg" alt="Beach slider Photo">
                <img class="slider-img" src="https://images.unsplash.com/photo-1580987502658-6b7345783e6f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=nick-fewings-IDU1v-rDmQ4-unsplash.jpg" alt="Beach slider Photo">

                <div class="slider-controls">
                    <i id="prev" class="fa fa-arrow-left"></i>
                    <i id="next" class="fa fa-arrow-right"></i>
                </div>

                <div class="slider-status-dots">
                    <span class="slider-dot slider-dot-active"></span>
                    <span class="slider-dot"></span>
                    <span class="slider-dot"></span>
                    <span class="slider-dot"></span>
                </div>
            </div>
            <div class="hero-content">
                <h1>UK Holidays, Short Breaks.</h1>
                <h1>Wonderful weeekends in Wales.</h1>

                <br />
                <a href="/accommodation.php" class="btn-primary">Find Out More</a>
            </div>
        </div>

        <div class="cards-section">
            <h2 class="section-title">Things to do.</h2>
            <div class="things-cards">
                <a href="/sports.php"><div class="card card-sports">
                    <h2>Sports<h2>
                </div></a>

                <a href="/accommodation.php"><div class="card card-accommodation">
                    <h2>Accommodation</h2>
                </div></a>

                <a href="/architecture.php"><div class="card card-architecture">
                    <h2>Architecture</h2>
                </div></a>

                <a href="/events.php"><div class="card card-events">
                    <h2>Events</h2>
                </div></a>
            </div>
        </div>

        <div class="contact-section">
            <div class="contact__info">
                <div class="contact__info__title">
                    <h2>Get Featured on</h2>
                    <img src="img/logo.png" alt="TravelWales Logo">
                </div>
                <p class="contact__info__p">
                    Want to get your business featured on our site? register your interest and we will get back to you. having your business listed on TravelWales opens you up to a huge number of potential customers.
                </p>
            </div>

            <div class="contact__form">
                <h1>Register Here</h1>
                <form name="contact" action="">
                    <fieldset aria-label="contact-info">
                        <legend>Contact Info:</legend>
                        <div class="contact__form__field">
                        <label for="name">Name: </label>
                        <input type="text" name="name" id="name">
                        </div>

                        <div class="contact__form__field">
                        <label for="email">Email: </label>
                        <input type="email" placeholder="example@example.com" name="email" id="email">
                        </div>
                   
                       <div class="contact__form__field">
                        <label for="website">Website: </label>
                        <input type="url" placeholder="https://example.com" name="website" id="website"> 
                        </div>
                    </fieldset>

                    <label for="category">Category:</label>
                    <select name="category" id="category">
                        <option value="Events">Events</option>
                        <option value="Sports">Sports</option>
                        <option value="Food">Food</option>
                        <option value="Architecture">Architecture</option>
                        <option value="Accommodation">Accommodation</option>
                    </select>

                    <label for="message">Message: </label>
                    <textarea aria-label="message" id="message" name="message" id="" cols="30" rows="10"></textarea>
                    <input type="submit" value="Register">
                </form>
            </div>
        </div>
    </body>
</html>
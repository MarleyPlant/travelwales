<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <?php include 'parts/head.php'; ?>
        <title>TravelWales - Sports</title>
    </head>

    <body>
        <?php include 'parts/header.php'; ?>

        <div class="post-list">
            <h1 class="post-list-title">Sports</h1>

            <?php
                $entry = [
                    'title' => "Cliff Jumping At Blue Lagoon",
                    'img'   => "img/bluelagoon.png",
                    'excerpt' => 'Flooded slate quarry known for bright blue-green water, near ruins of worker huts & a pebble beach. this is the perfect spot to get your adrenaline pumping with amazing cliff jumps & beautiful scenary',
                    'website' => 'https://bluelagoon,net',
                    'distance' => ['51.93790847153342','-5.20950320233569'],
                    'directionsLink' => ''
                ];

                include 'parts/postlist-entry.php';

                $entry = [
                    'title' => "Urdd Gobaith Cymru (Llangrannog)",
                    'img'   => "https://providerfiles.thedms.co.uk/eandapics/MW/iphone/1010174_1_1.jpg",
                    'excerpt' => "The Urdd Residential Center offers an opportunity to experience the attractions of the Welsh capital whilst staying in the unique Wales Millennium Centre, one of the world's premier arts venues.",
                    'website' => 'https://bluelagoon,net',
                    'distance' => ['52.16539105965592','-4.446965728907372'],
                    'directionsLink' => ''
                ];

                include 'parts/postlist-entry.php';
            ?>


        </div>
    </body>
</html>
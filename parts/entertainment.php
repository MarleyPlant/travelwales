<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <?php include 'parts/head.php'; ?>
    </head>


    <body>
        <?php include 'parts/header.php'; ?>

        <div class="post-list">
            <h1 class="post-list-title">Entertainment</h1>
            <div class="post">
                <img class="post-img" src="img/bluelagoon.png" alt="" srcset="">
                <div class="post-content">
                    <h2 class="post-content-title">Blue Lagoon</h2>
                    <p class="post-content-excerpt">
                        Dolor ad ea incididunt pariatur culpa cupidatat quis velit quis id velit reprehenderit excepteur. Qui tempor veniam eiusmod tempor incididunt ut culpa quis eiusmod. In nulla et consectetur non nostrud consectetur irure sit irure ad ea eiusmod. Do pariatur exercitation minim fugiat proident irure ullamco officia qui ut laboris. Sit commodo ipsum eu culpa consectetur. Magna velit do aliqua officia amet veniam dolore pariatur occaecat officia id fugiat. Velit officia velit ad ipsum exercitation eiusmod eiusmod eiusmod laboris deserunt.
                    </p>

                    <div class="post-info">
                        <a class="btn btn-secondary" href=""> <i class="fas fa-map"></i> Get Directions</a>
                        <i class="post-info-distance">44km Away</i>
                    </div>
                </div>
            </div>

            <div class="post">
                <img class="post-img" src="img/bluelagoon.png" alt="" srcset="">
                <div class="post-content">
                    <h2 class="post-content-title">Blue Lagoon</h2>
                    <p class="post-content-excerpt">
                        Dolor ad ea incididunt pariatur culpa cupidatat quis velit quis id velit reprehenderit excepteur. Qui tempor veniam eiusmod tempor incididunt ut culpa quis eiusmod. In nulla et consectetur non nostrud consectetur irure sit irure ad ea eiusmod. Do pariatur exercitation minim fugiat proident irure ullamco officia qui ut laboris. Sit commodo ipsum eu culpa consectetur. Magna velit do aliqua officia amet veniam dolore pariatur occaecat officia id fugiat. Velit officia velit ad ipsum exercitation eiusmod eiusmod eiusmod laboris deserunt.
                    </p>

                    <div class="post-info">
                        <a class="btn btn-secondary" href=""> <i class="fas fa-map"></i> Get Directions</a>
                        <i class="post-info-distance">44km Away</i>
                    </div>
                </div>
            </div>

            <div class="post">
                <img class="post-img" src="img/bluelagoon.png" alt="" srcset="">
                <div class="post-content">
                    <h2 class="post-content-title">Blue Lagoon</h2>
                    <p class="post-content-excerpt">
                        Dolor ad ea incididunt pariatur culpa cupidatat quis velit quis id velit reprehenderit excepteur. Qui tempor veniam eiusmod tempor incididunt ut culpa quis eiusmod. In nulla et consectetur non nostrud consectetur irure sit irure ad ea eiusmod. Do pariatur exercitation minim fugiat proident irure ullamco officia qui ut laboris. Sit commodo ipsum eu culpa consectetur. Magna velit do aliqua officia amet veniam dolore pariatur occaecat officia id fugiat. Velit officia velit ad ipsum exercitation eiusmod eiusmod eiusmod laboris deserunt.
                    </p>

                    <div class="post-info">
                        <a class="btn btn-secondary" href=""> <i class="fas fa-map"></i> Get Directions</a>
                        <i class="post-info-distance">44km Away</i>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
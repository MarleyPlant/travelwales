<?php
    $coordsString = $entry['distance'][0] . ", " . $entry['distance'][1];
    $directionsLink = 'https://www.google.com/maps/dir/?api=1&destination=' . $coordsString;
?>

<div class="post">
    <img alt="<?= $entry['title'] ?>-postlist-img" class="post-img" src="<?= $entry['img'] ?>">
    <div class="post-content">
        <h2 class="post-content-title"><?= $entry['title'] ?></h2>
        <p class="post-content-excerpt">
            <?= $entry['excerpt'] ?>
        </p>

        <div class="post-info">
            <a class="btn btn-secondary" href="<?= $entry['website'] ?>"> <i class="fas fa-globe"></i> Website</a>
            <a class="btn btn-secondary" href="<?= $directionsLink ?>"> <i class="fas fa-map"></i> Get Directions</a>
            <i class="post-info-distance"><?= $coordsString ?></i>
        </div>
    </div>
</div>
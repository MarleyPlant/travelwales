<header class="navbar">
    <a href="/index.php"><img alt="TravelWales Logo" class="navbar-logo" src="img/logo.png" /></a>
    
    <div class="navbar-links">
        <a href="/events.php">Events</a>
        <a href="/sports.php">Sports</a>
        <a href="/food.php">Food</a>
        <a href="/architecture.php">Architecture</a>
        <a href="/accommodation.php">Accommodation</a>
        <!-- <a href="/contact.php">Contact Us</a> -->
    </div>

    <a class="mobile-nav-icon"><i class="fas fa-bars"></i></a>
</header>
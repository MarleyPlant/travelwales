<link rel="shortcut icon" href="icon/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="scss/index.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="js/distance.js"></script>
<script src="js/form.js"></script>
<script src="js/navbar.js"></script>
<script src="js/slider.js"></script>
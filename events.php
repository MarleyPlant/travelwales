<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <?php include 'parts/head.php'; ?>
        <title>TravelWales - Events</title>

    </head>


    <body>
        <?php include 'parts/header.php'; ?>

        <div class="post-list">
            <h1 class="post-list-title">Events</h1>
            
            <?php
                $entry = [
                    'title' => "Balter",
                    'img'   => "https://balterfestival.com/wordpress/wp-content/uploads/2019/10/hero-tickets.jpg",
                    'excerpt' => 'Balter Festival 2021will be the 7th gathering of the most exciting underground sounds. Join us at Chepstow Racecourse',
                    'website' => 'https://balterfestival.com/',
                    'distance' => ['51.93807319694512','-5.20910685576357'],
                    'directionsLink' => ''
                ];

                include 'parts/postlist-entry.php';

                $entry = [
                    'title' => "Fire In The Mountain",
                    'img'   => "https://www.metalsucks.net/wp-content/uploads/2018/08/239726BF-F2A2-4192-B651-E35B8A44D26E-1000x515.jpeg",
                    'excerpt' => 'Red Kites soar above lush green ravines, with oak grove and wild meadow below. Located between the fringe of the magnificent Cambrian mountains and the beautiful coast of Cardigan Bay, where hare and horse graze, natures spirits ebbing and flowing.
                    Join us in bringing this ancient kingdom alive in magical gatherings of hearts, minds and music',
                    'website' => 'http://www.fireinthemountain.co.uk/',
                    'distance' => ['52.3469','3.9632'],
                    'directionsLink' => ''
                ];

                include 'parts/postlist-entry.php';

                $entry = [
                    'title' => "Landed Festival",
                    'img'   => "https://cdn2.fatsoma.com/media/W1siZiIsInB1YmxpYy8yMDIxLzMvMy8xMi8yOC80MS8zNS8xNTU3OTU2MjBfMjUwMDY5NjIzNDkxNzI3XzYwODYxMjg4ODk2MDc4Mzk4NTNfbi5qcGciXV0",
                    'excerpt' => 'Winner BEST WELSH FESTIVAL 2019. Early birds Tickets / SOLD OUT. Full Price Weekend is £100 online, plus booking fee. Pub prices, affordable ...',
                    'website' => 'http://www.landedfestival.co.uk/',
                    'distance' => ['52.2531','3.4722'],
                    'directionsLink' => ''
                ];

                include 'parts/postlist-entry.php';
                
                $entry = [
                    'title' => "Blackhouse",
                    'img'   => "https://fatsoma.imgix.net/W1siZiIsInB1YmxpYy8yMDE3LzIvMTUvMjAvNTEvNi84NzAvQmxhY2stSG91c2UtY292ZXItcmVhbGx5c21hbGwucG5nIl1d?w=559&h=336&fit=min&auto=format%2Ccompress",
                    'excerpt' => 'BLACK HOUSE - Mid Wales biggest night of the year. A multi stage, multi floor and multi genre indoor festival event, celebrating dance music from across the board, our local independent scene and sound system culture.',
                    'website' => 'https://www.facebook.com/blackhouseuk/',
                    'distance' => ['52.41522554046413','-4.082279718536427'], 
                    'directionsLink' => ''
                ];

                include 'parts/postlist-entry.php';
            ?>
        </div>
    </body>
</html>